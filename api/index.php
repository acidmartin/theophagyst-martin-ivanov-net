<?php
/**
 * API routes
 */
$request = $_SERVER['REQUEST_URI'];
$requestHost = $_SERVER['HTTP_HOST'];

if (strpos($requestHost, 'localhost') !== false) {
  header('Access-Control-Allow-Origin: *');
  header('Access-Control-Allow-Credentials: true');
  header('Access-Control-Allow-Methods: POST, OPTIONS, GET');
  header('Access-Control-Allow-Headers: Origin, X-Custom-Header, X-Api-Key, X-Requested-With, Content-Type, Accept, Authorization');
}

ob_start('ob_gzhandler');

require_once 'api.php';
$routes = json_decode(file_get_contents('./routes.json'), true);
$apiRoot = '/api/';

if (strpos($requestHost, 'localhost') !== false) {
    $apiRoot = '/theophagyst-martin-ivanov-net/api/';
}

if ($request === $apiRoot) {
  die(json_encode(array(
    'status' => 'success'
  )));
}

$api = new Api();

switch ($request) {
  case $apiRoot . $routes['routes']['url']:
    $api -> routes();
    break;
  case $apiRoot . $routes['home-video']['url']:
    $api -> homeVideo();
    break;
  case $apiRoot . $routes['footer-player']['url']:
    $api -> footerPlayer();
    break;
  case $apiRoot . $routes['blogposts']['url']:
    $api -> blogposts();
    break;
  case $apiRoot . $routes['videos']['url']:
    $api -> videos();
    break;
  case $apiRoot . $routes['gear']['url']:
    $api -> gear();
    break;
  case $apiRoot . $routes['page']['url']:
    $api -> page();
    break;
  case $apiRoot . $routes['post']['url']:
    $api -> post();
    break;
  case $apiRoot . $routes['contact']['url']:
    $api -> contact();
    break;
  case $apiRoot . $routes['video-categories']['url']:
    $api -> getVideoCategories();
    break;
  case $apiRoot . $routes['config']['url']:
    $api -> getConfig();
    break;
}
