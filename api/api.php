<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require './lib/PHPMailer/src/Exception.php';
require './lib/PHPMailer/src/PHPMailer.php';
require './lib/PHPMailer/src/SMTP.php';

/**
 * @class Api
 * @author Martin Ivanov
 */
class Api {

  /**
   * SMTP/POP3 Settings
   */
  static $smtpPort = 587;
  static $smtpTransportLayer = 'tls';
  static $smtpServer = 'wemakesites.net';
  static $smtpUsername = 'admin@wemakesites.net';
  static $smtpPassword = 'cenobite1561';
  static $smtpDefaultToEmail = 'acid_martin@yahoo.com';
  static $fromName = 'Theophagyst';
  static $me = 'Martin Ivanov';

  static $domain = 'https://theophagyst.martinivanov.net';

  static $blogPostItemsPerPage = 10;

  static function getVideos () {
    return json_decode(file_get_contents('./data/videos.json'));
  }

  static function getVideoCategoriesData () {
    return json_decode(file_get_contents('./data/video-categories.json'));
  }

  static function getConfigData () {
    return json_decode(file_get_contents('./data/config.json'));
  }

  /**
   * Get list of the available API methods
   * @method routes
   * @return string
   */
  public function routes () {
    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
      $data = json_decode(file_get_contents('./routes.json'));
      Api::success('success', $data);
    } else {
      Api::error();
    }
  }

  public function getConfig () {
    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
      Api::success('success', Api::getConfigData());
    } else {
      Api::error();
    }
  }

  public function getVideoCategories () {
    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
      Api::success('success', Api::getVideoCategoriesData());
    } else {
      Api::error();
    }
  }

  public function videos () {
    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
      Api::success('success', Api::getVideos());
    } else {
      Api::error();
    }
  }

  public function contact () {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $data = json_decode(file_get_contents('php://input'), true);
      $subject = 'Contact form results';
      /**
       * send the form to our email
       */
      $contactFormMessageTemplate = '<p>Hey Martin,</p>
<p>You have just received a message from the <a href="' . Api::$domain . '">Theophagyst</a> website. Message details:</p>
<p>
  <strong>from:</strong> ' . $data['name'] . '<br />
  <strong>email:</strong> <a href="mailto:'. $data['email'] .'">'. $data['email'] .'</a><br />
  <strong>message:</strong> ' . strip_tags($data['message']) . '</p>
  <p>Yours,<br />Theophagyst<br /><a href="' . Api::$domain . '">' . Api::$domain . '</a></p>';
      Api::email(Api::$fromName, $subject, $data['email'], $data['name'], $contactFormMessageTemplate, Api::$smtpDefaultToEmail);
      Api::success('message sent', null);
    } else {
      Api::error();
    }
  }

  /**
   * Generic send email function
   * @static email
   * @param string $emailFromName
   * @param string $emailSubject
   * @param string $emailReplyTo
   * @param string $emailReplyToName
   * @param string $emailBody
   * @param string $emailTo
   * @return string
   */
  static function email ($emailFromName, $emailSubject, $emailReplyTo, $emailReplyToName, $emailBody, $emailTo) {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $mail = new PHPMailer(true);
      $mail -> SMTPDebug = 0;
      $mail -> isSMTP();
      $mail -> CharSet = 'UTF-8';
      $mail -> Host = Api::$smtpServer;
      $mail -> SMTPAuth = true;
      $mail -> Username = Api::$smtpUsername;
      $mail -> Password = Api::$smtpPassword;
      $mail -> SMTPSecure = Api::$smtpTransportLayer;
      $mail -> Port = Api::$smtpPort;
      $mail -> setFrom(Api::$smtpUsername, $emailFromName);
      $mail -> addReplyTo($emailReplyTo, $emailReplyToName);
      $mail -> isHTML(true);
      $mail -> Subject = $emailSubject;
      $mail -> Body = $emailBody;
      $mail -> AltBody = strip_tags($emailBody);
      $mail -> addAddress($emailTo);
      $mail -> send();
    } else {
      Api::error();
    }
  }

  public function post () {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $slug = json_decode(file_get_contents('php://input'), true)['slug'];
      $post = json_decode(file_get_contents('https://api.martinivanov.net/api-proxy.php?resource=get_post&post_slug=' . $slug), true);
      if ($post) {
        $data = array(
          'slug' => $slug,
          'post' => $post
        );
        Api::success('success', $data);
      } else {
        Api::error(4, 'post does not exist');
      }
    } else {
      Api::error();
    }
  }

  public function page () {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $slug = json_decode(file_get_contents('php://input'), true)['slug'];
      $htmlFile = '../static/pages/' . $slug . '.html';
      $jsonFile = '../static/meta/' . $slug . '.json';
      if (file_exists($htmlFile) && file_exists($jsonFile)) {
        $content = file_get_contents($htmlFile);
        $meta = json_decode(file_get_contents($jsonFile), true);
        $data = array(
          'slug' => $slug,
          'content' => $content,
          'meta' => $meta
        );
        Api::success('success', $data);
      } else {
        Api::error(2, 'requested page does not exist');
      }
    } else {
      Api::error();
    }
  }

  public function gear () {
    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
      $data = json_decode(file_get_contents('https://wemakesites.net/api/pages/the-guitars'), true);
      $data = $data['page']['guitars']['thumbs'];
      Api::success('success', $data);
    } else if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $slug = json_decode(file_get_contents('php://input'), true)['slug'];
      $data = json_decode(file_get_contents('https://wemakesites.net/api/data/gear/' . $slug), true);
      $images = json_decode(file_get_contents('https://wemakesites.net/api/data/gearpictures/' . $slug), true);
      $data['data']['images'] = $images['data'];
      Api::success('success', $data['data']);
    } else {
      Api::error();
    }
  }

  public function homeVideo () {
    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
      $data = json_decode(file_get_contents('./data/videos.json'), true);
      $onHomePage = [];
      foreach ($data as $video) {
        if ($video['onHomePage']) {
          $onHomePage[] = $video;
        }
      }
      $onHomePage = $onHomePage[array_rand($onHomePage)];
      Api::success('success', $onHomePage);
    } else {
      Api::error();
    }
  }

  public function blogposts () {
    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
      $itemsPerPage = Api::$blogPostItemsPerPage;
      $data = json_decode(file_get_contents('https://api.martinivanov.net/wp-json/wp/v2/posts?categories=1269,65,101&per_page=' . $itemsPerPage), true);
      Api::success('success', $data);
    } else {
      Api::error();
    }
  }

  public function footerPlayer () {
    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
      $data = json_decode(file_get_contents('./data/footer-player.json'));
      $data = $data[array_rand($data)];
      Api::success('success', $data);
    } else {
      Api::error();
    }
  }

  /**
   * Return success status
   * @method success
   * @param $message string
   * @param $data object
   * @return string
   * }
   */
  public function success ($message = 'success', $data = null) {
    echo json_encode(array(
      'code' => 0,
      'status' => 'success',
      'message' => $message,
      'data' => $data
    ));
  }

  /**
   * Return error status
   * @method error
   * @param $code int
   * @param $message string
   * @param $data object
   * @return string
   * }
   */
  public function error ($code = 1, $message = 'method not allowed', $data = null) {
    echo json_encode(array(
      'code' => $code,
      'status' => 'fail',
      'message' => $message,
      'data' => $data
    ));
  }
}
