import config from '../helpers/config'

const methods = {
  methods: {
    makeBandCampPlayerUrl (albumId) {
      const colors = this.$config.bandCampPlayer
      return `https://bandcamp.com/EmbeddedPlayer/album=${albumId}/size=large/artwork=small/bgcol=${colors.bgcol}/linkcol=${colors.linkcol}/transparent=true/`
    },
    getAlbumDetails (slug) {
      return this.$config.albumDetails[slug]
    },
    getRandomVideo (usedVideosList = 'featuredVideos', filterBy = 'featured') {
      const videos = this.$root.videos
      const usedVideosLength = videos.filter(video => video[filterBy]).length
      const usedVideos = this.$root[usedVideosList]
      this.video = this.arrayRand(videos.filter(video => video[filterBy] && !usedVideos.includes(video.id))) || null
      if (this.video) {
        usedVideos.push(this.video.id)
      }
      if (usedVideosLength === usedVideos.length) {
        this.$root[usedVideosList] = []
        this.getRandomVideo(usedVideosList, filterBy)
      }
    },
    showImageInLightbox (src) {
      this.$root.$emit('show-image-in-lightbox', ({src}))
    },
    openVcAudioPlayer (albumId = '', trackId = '', name = 'vc-audio-player') {
      const url = `https://vc-audio-player.martinivanov.net/player?album=${albumId}${trackId ? '&track=' + trackId : ''}`
      const height = 640
      const width = 480
      const left = (screen.width / 2) - (width / 2)
      const top = (screen.height / 2) - (height / 2)
      const newWindow = window.open(url, name, `height=640, width=480, left=${left}, top=${top}`)
      this.$root.albumIdForTheTrackPage = albumId
      if (window.focus) {
        newWindow.focus()
      }
      return false
    },
    gtagEvent (action = '', category = '', label = '', value = '') {
      this.$gtag.event(action, {
        'event_category': category,
        'event_label': label,
        'value': value
      })
    },
    oneMonthFromNow () {
      return new Date(Date.now() + 2.628e+9)
    },
    getVideoById (id) {
      const video = this.$root.videos.find(video => video.id === id)
      return video
    },
    makeSingleVideoUrl (id) {
      const title = this.makeSlug(this.getVideoById(id).title)
      return `/videos/${title}`
    },
    makeAlbumUri (slug) {
      return `/music/${slug}`
    },
    makeSlug (title) {
      return title.replace(/\s/g, '-').replace(':', '').replace('/', '-').toLowerCase()
    },
    cacheData (slug, where, data) {
      this.$root.cache[where][slug] = data
    },
    retrieveCachedData (where, slug) {
      return this.$root.cache[where][slug]
    },
    makeFacebookListPlayerUrl (id) {
      const config = this.$config
      return `${config.facebookVideoPlayerRoot}/${config.bcArtistSlug}/videos/${id}/${config.facebookVideoPlayerUrlParams}`
    },
    makePlayerUrl (slug) {
      let video = this.$root.videos.find(video => this.makeSlug(video.title) === slug)
      const config = this.$config
      const params = config.facebookVideoPlayerUrlParams
      const artistSlug = config.bcArtistSlug
      const fbVideosRoot = config.facebookVideoPlayerRoot
      if (video) {
        video = video.id
        return `${fbVideosRoot}/${artistSlug}/videos/${video}/${params}`
      } else {
        return `${fbVideosRoot}/${artistSlug}/videos/1234567890/${params}`
      }
    },
    setPosts (data) {
      data.forEach(post => {
        let img
        const temp = document.createElement('div')
        temp.innerHTML = post.content.rendered
        img = temp.querySelector('img')
        post.image = img ? img.getAttribute('src') : this.$config.replacementImage
        post.excerpt.rendered = this.removeHtmlTagsFromString(post.excerpt.rendered)
      })
      this.posts = data
    },
    arrayRand (array = []) {
      return array[Math.floor(Math.random() * array.length)]
    },
    getPosts () {
      const slug = this.$route.path
      const cachedData = this.retrieveCachedData('blog', slug)
      if (cachedData) {
        return this.setPosts(cachedData)
      }
      this.$http({
        url: this.getRoute('blogposts').url
      }).then(response => {
        const data = response.data.data
        this.setPosts(data)
        this.cacheData(slug, 'blog', data)
      }).catch(error => {
        console.log(error)
      })
    },
    getRoute (route) {
      route = this.$root.routes[route]
      if (!route.hasApiRoot) {
        route.url = `${config.apiRoot}/${route.url}`
        route.hasApiRoot = true
      }
      return route
    },
    removeHtmlTagsFromString (str) {
      if ((str === null) || (str === '')) {
        return false
      } else {
        str = str.toString()
      }
      return str.replace(/(<([^>]+)>)/ig, '')
    },
    getBCRoute (route) {
      return `${this.$config.bandcampApiRoot}/${route}`
    },
    getFooterPlayerTrack () {
      const url = this.getRoute('footer-player').url
      this.$http({
        url
      }).then(response => {
        this.$root.$emit('footer-player', response.data.data)
      }).catch(error => {
        console.error(error)
      })
    }
  }
}

export default methods
