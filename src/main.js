import Vue from 'vue'
import App from './app'
import Vuetify from 'vuetify'
import axios from 'axios'
import VueAxios from 'vue-axios'
import router from './helpers/router'
import VueGtag from 'vue-gtag'
import Cookies from 'universal-cookie'

import methods from './mixins/methods'
import lang from './helpers/lang'
import config from './helpers/config'

import routes from '../api/routes'

import 'vuetify/dist/vuetify.min.css'

import HomepageVideo from './components/homepage-video'
import FloatingPlayer from './components/floating-player'
import StaticPageContent from './components/static-page-content'
import VcStyle from './components-third-party/vc-style/vc-style'
import VcAudioPlayer from './components-third-party/vc-audio-player/vc-audio-player'
import SocialServices from './components/social-services'
import ContactForm from './components/contact-form'
import NewsTicker from './components/news-ticker'
import FeaturedVideo from './components/featured-video'
import OnPageVcAudioPlayer from './components/on-page-vc-audio-player'
import Lightbox from './components/lightbox'
import BuyAlbum from './components/buy-album'
import AlbumPlayer from './components/album-player'

Vue.config.productionTip = false

Vue.use(Vuetify)
Vue.use(VueAxios, axios)
Vue.use(VueGtag, {
  config: {
    id: config.googleAnalyticsId,
    params: {
      send_page_view: false
    }
  }
}, router)

Vue.prototype.$lang = lang
Vue.prototype.$config = config

Vue.component('homepage-video', HomepageVideo)
Vue.component('floating-player', FloatingPlayer)
Vue.component('static-page-content', StaticPageContent)
Vue.component('vc-style', VcStyle)
Vue.component('social-services', SocialServices)
Vue.component('contact-form', ContactForm)
Vue.component('news-ticker', NewsTicker)
Vue.component('featured-video', FeaturedVideo)
Vue.component('featured-video', FeaturedVideo)
Vue.component('vc-audio-player', VcAudioPlayer)
Vue.component('on-page-vc-audio-player', OnPageVcAudioPlayer)
Vue.component('lightbox', Lightbox)
Vue.component('buy-album', BuyAlbum)
Vue.component('album-player', AlbumPlayer)

/* eslint-disable no-new */
new Vue({
  router,
  el: '#app',
  template: '<App/>',
  components: {
    App
  },
  mixins: [
    methods
  ],
  data () {
    return {
      routes,
      config,
      ready: false,
      ajax: true,
      notification: false,
      notificationText: '',
      cookieConsent: new Cookies().get(config.cookieConsent),
      playerMinimized: false,
      cache: {
        static: {},
        blog: {},
        gear: {},
        music: {},
        videos: {},
        player: {}
      },
      videos: [], // array of all available videos from the API
      featuredVideos: [], // array of already featured videos
      homepageVideos: [], // array of videos already shown on the home page
      albumIdForTheTrackPage: null
    }
  },
  watch: {
    '$route.path' () {
      this.scrollToTop()
    },
    '$route' (to, from, next) {
      if (to.name === 'merch') {
        window.open(this.$config.merchSite, '_blank')
        this.$router.push({
          path: from.path
        })
      }
    }
  },
  created () {
    this.axiosSetup()
    if (this.$config.dark) {
      document.querySelector('body').classList.add('body--theme--dark')
    }
    this.$on('play', (data) => {
      this.gtagEvent('play', 'track', data.title)
    })
  },
  methods: {
    getVideos () {
      const url = this.getRoute('videos').url
      this.$http({
        url
      }).then(response => {
        this.videos = response.data.data
        this.cache.videos['/videos'] = this.videos
        this.ready = true
        this.$nextTick(() => {
          this.getFooterPlayerTrack()
        })
      }).catch(error => {
        console.error(error)
      })
    },
    scrollToTop () {
      window.scrollTo(0, 0)
    },
    axiosSetup () {
      const http = this.$http
      http.defaults.headers.common = {
        'X-Api-Key': this.$config.bandcampApiKey
      }
      http.interceptors.request.use(config => {
        this.ajax = true
        return config
      }, function (error) {
        this.ajax = false
        return Promise.reject(error)
      })
      http.interceptors.response.use(response => {
        this.ajax = false
        this.scrollToTop()
        return response
      }, (error) => {
        return Promise.reject(error)
      })
      this.getPurchaseOptions()
    },
    getPurchaseOptions () {
      const url = this.getRoute('config').url
      this.$http({url}).then(response => {
        const data = response.data.data
        Object.assign(this.config, data)
        this.$lang.titleFixes = data.titleFixes
        this.$nextTick(() => {
          this.getVideos()
        })
        // console.log(this.config)
      }).catch(error => {
        console.error(error)
      })
    }
  }
})
