/**
 * String validation rules
 * @module value
 */

import lang from './lang'

const value = [
  value => !!value || lang.required.toUpperCase()
]

export default value
