import config from './config'
import moment from 'moment'

const lang = {
  name: 'Name',
  email: 'Email',
  message: 'Message',
  player: 'Player',
  close: 'Close',
  closePlayer: 'Close player',
  submit: 'Submit Form',
  required: 'Required',
  dragPlayer: 'Drag player',
  ok: 'Ok',
  blog: 'Blog',
  cookiePolicy: 'Policy',
  allVideos: 'All Videos',
  goToMerch: 'Visit My Merch Site',
  weUseCookies: 'I use cookies to ensure you get the best experience',
  playerMinimized: 'Player minimized in footer',
  invalidEmailFormat: 'Invalid email format',
  messageSent: 'Message sent. Thank you for contacting me! I will get in touch with you soon.',
  fromTheAlbum: 'From the album',
  guitars: 'My Guitar Fleet',
  amps: 'Amps, Cabinets and Monitors',
  fx: 'Effects and Pedals',
  recorders: 'Recorders and Studio Equipment',
  misc: 'Miscellaneous',
  playTrack: 'Play Track',
  videosFromTheAlbum: 'Videos from the album',
  playAlbumInNewWindow: 'Play in new window',
  gearNotFound: 'The gear you requested was not found. Hit the back button of your browser and try another link.',
  viewTrackInfo: 'Listen and view track info',
  playTrackInNewWindow: 'Play track in new window',
  errors: {
    '2': 'Album not found. Hit the back button of your browser and try another album.',
    '3': 'Track not found. Hit the back button of your browser and try another track.',
    '4': 'The requested post does not exist. Hit the back button of your browser and try another one.'
  },
  featuredVideo: (title) => {
    return `<strong class="hidden-sm-and-down">Featured Video:&nbsp;</strong>${title}`
  },
  latestNews: (news) => {
    return `<strong class="hidden-sm-and-down">Latest News:&nbsp;</strong>${news}`
  },
  play: (title) => {
    return `Play${title ? ' ' + title : ''}`
  },
  thankYouForYourPurchase (title) {
    return `Thank you for supporting my music endeavours! Click the button below to download "${title}" from Google Drive.`
  },
  downloadAlbum (title) {
    return `Download "${title.replace(/&#8203;ÿ&#8203;/, 'ÿ')}" from Google Drive`
  },
  payPalItemTitle: (title) => {
    return encodeURIComponent(`${title} by Theophagÿst`)
  },
  buyAlbumViaPayPal: (title) => {
    return `Buy "${title}" from BandCamp (instant digital download)`
  },
  watchOnFacebook: (title) => {
    return `Watch${title ? ' ' + title : ''} on Facebook`
  },
  postDetails: (firstName, lastName, date, url) => {
    date = moment(date).format(config.dateFormat)
    return `Posted by <a href="${config.blog}" target="_blank" title="${firstName} ${lastName}" rel="noreferrer">${firstName} ${lastName}</a> on <a rel="noreferrer" class="white--text" title="${date}" href="${url}" target="_blank">${date}</a>`
  },
  viewRelease: (title) => {
    return `View and listen to ${title}`
  },
  playAlbum: (title) => {
    return `Play${title ? ' ' + title : ' album'}`
  },
  togglePlayer (expanded) {
    return `${expanded ? 'Minimize' : 'Maximize'} player${expanded ? ' in the footer' : ''}`
  },
  continueReading (title) {
    return `Continue reading${title ? ' ' + title : ''}`
  },
  viewGear (caption) {
    return `View ${caption}`
  },
  shareOn: (service) => {
    return `Share via ${service}`
  },
  buyAlbum: () => {
    return `Buy on BandCamp`
  },
  albumInProgress (data) {
    const year = data.year
    return data.inProgress ? `${year} <span class="opacity-50">(In Progress)</span>` : year
  }
}

export default lang
