const links = [{
  url: 'https://theophagyst.bandcamp.com',
  label: 'Follow me on Bandcamp',
  icon: 'mdi-bandcamp'
}, {
  url: 'https://martinivanov.net',
  label: 'Blog',
  icon: 'mdi-book-open-page-variant'
}, {
  url: 'https://twitter.com/vuecidity',
  label: 'Follow me on Twitter',
  icon: 'mdi-twitter'
}, {
  url: 'https://facebook.com/theophagyst',
  label: 'Follow me on Facebook',
  icon: 'mdi-facebook'
}, {
  url: 'https://www.linkedin.com/in/martin-krasimirov-ivanov/',
  label: 'Follow me on LinkedIn',
  icon: 'mdi-linkedin-box'
}, {
  url: 'https://wemakesites.net',
  label: 'My personal website',
  icon: 'mdi-link'
}]

export default links
