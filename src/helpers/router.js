import Vue from 'vue'
import Router from 'vue-router'
import PageIndex from '@/pages/index'
import PageAbout from '@/pages/about'
import PageMusic from '@/pages/music'
import PageMusicAlbum from '@/pages/music-album'
import PageMusicAlbumTrack from '@/pages/music-album-track'
import PageVideos from '@/pages/videos'
import PageVideosVideo from '@/pages/videos-video'
import PageGear from '@/pages/gear'
import PageGearItem from '@/pages/gear-item'
import PageContact from '@/pages/contact'
import PageCookiePolicy from '@/pages/cookie-policy'
import PageBlog from '@/pages/blog'
import PageBlogPost from '@/pages/blog-post'
import PageMerch from '@/pages/merch'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [{
    path: '/',
    name: 'index',
    component: PageIndex
  }, {
    path: '/about',
    name: 'about',
    component: PageAbout
  }, {
    path: '/music',
    name: 'music',
    component: PageMusic
  }, {
    path: '/music/:album',
    name: 'music-album',
    component: PageMusicAlbum
  }, {
    path: '/music/:album/:track',
    name: 'music-album-track',
    component: PageMusicAlbumTrack
  }, {
    path: '/videos',
    name: 'videos',
    component: PageVideos
  }, {
    path: '/videos/:slug',
    name: 'videos-video',
    component: PageVideosVideo
  }, {
    path: '/gear',
    name: 'gear',
    component: PageGear
  }, {
    path: '/gear/:slug',
    name: 'gear-item',
    component: PageGearItem
  }, {
    path: '/contact',
    name: 'contact',
    component: PageContact
  }, {
    path: '/cookie-policy',
    name: 'cookie-policy',
    component: PageCookiePolicy
  }, {
    path: '/blog',
    name: 'blog',
    component: PageBlog
  }, {
    path: '/blog/:post',
    name: 'blog-post',
    component: PageBlogPost
  }, {
    path: '*',
    redirect: '/'
  }, {
    path: '/merch',
    name: 'merch',
    component: PageMerch
  }]
})
