import {
  appName
} from '../../package'

const headerMenu = [{
  label: 'About',
  hint: 'About me and my music project Theophagÿst',
  url: '/about',
  icon: 'mdi-information-outline'
}, {
  label: 'Music',
  hint: 'The music of Theophagÿst',
  url: '/music',
  icon: 'mdi-music-note'
}, {
  label: 'Videos',
  hint: 'Videos',
  url: '/videos',
  icon: 'mdi-video'
}, {
  label: appName,
  hint: appName,
  url: '/',
  icon: 'mdi-home'
}, {
  label: 'Gear',
  hint: 'My guitars, gear and studio equipment',
  url: '/gear',
  icon: 'mdi-guitar-pick'
}, {
  label: 'Blog',
  hint: 'Blog and latest news',
  url: '/blog',
  icon: 'mdi-wordpress'
}, {
  label: 'Contact',
  hint: 'Contact me',
  url: '/contact',
  icon: 'mdi-email'
}]

export default headerMenu
