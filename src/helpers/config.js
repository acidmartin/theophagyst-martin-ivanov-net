import {
  domain,
  appName,
  appFullName
} from '../../package'

const apiRoot = window.location.href.indexOf('localhost') > -1 ? 'http://localhost:8888/theophagyst-martin-ivanov-net/api' : `${domain}/api`

const config = {
  domain,
  appName,
  apiRoot,
  appFullName,
  dark: true,
  googleAnalyticsId: 'G-QWDYJEPQYG',
  dateFormat: 'LL',
  cookieConsent: '__cookie_consent',
  cookieConsentEnabled: false,
  bcArtistSlug: 'theophagyst',
  mainSite: 'https://wemakesites.net',
  merchSite: 'https://theophagyst-store.martinivanov.net/',
  blog: 'https://martinivanov.net',
  aboutPagePhoto: '/static/images/about-page-photo.jpg',
  bandcampApiRoot: 'https://bc.wemakesites.net/api',
  bandcampApiKey: '14804fe005220d42595e1c2adb65c658',
  replacementImage: '/static/images/replacement-image.jpg',
  facebookVideoPlayerUrlParams: '&show_text=false&',
  facebookVideoPlayerRoot: 'https://www.facebook.com/plugins/video.php?height=314&href=https://www.facebook.com',
  colors: {
    primary: 'deep-purple',
    hex: '#7e57c2',
    success: 'green',
    danger: 'red',
    lighter: [
      'deep-purple',
      'lighten-2'
    ].join(' ')
  }
}

export default config
