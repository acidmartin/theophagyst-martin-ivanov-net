<?php
ob_start('ob_gzhandler');
$requestUri = $_SERVER['REQUEST_URI'];
$urlParts = explode('/', $requestUri);
$allCountries = 'all countries';
$globalShoppingDirectory = 'Global Shopping Directory';
$mainUrl = 'https://theophagyst.martinivanov.net';
$mainTitle = 'Theophagÿst';
$titleDelimiter = ' | ';
$me = 'Martin Ivanov';
$categories = array('about', 'music', 'videos', 'gear', 'contact', 'blog');

foreach ($urlParts as $part) {
  if ($part) {
    $title = $title . $titleDelimiter . str_replace('-', ' ', str_replace('%20', ' ', $part));
  }
}

$title = ucwords(str_replace('-',' ', $mainTitle . $title));

$metaData = array(
  'keywords' => array(),
  'description' => ''
);
$jsonFile = './static/meta/' . end($urlParts) . '.json';
if (end($urlParts) && file_exists($jsonFile)) {
  $metaData = json_decode(file_get_contents($jsonFile), true);
} else if (end($urlParts) === '') {
  $metaData = json_decode(file_get_contents('./static/meta/index.json'), true);
}
$keywords = implode(', ', $metaData['keywords']);
$description = $metaData['description'];
if ($description) {
  $description = str_replace('"', '', $description);
}
$contextualOgImage = './static/images/' . end($urlParts) . '.jpg';
if (file_exists($contextualOgImage)) {
  $ogImage = $mainUrl . str_replace('./', '/', $contextualOgImage);
} else {
  $ogImage = $mainUrl . '/static/images/og-image.jpg';
}
?>
<!doctype html>
<html lang="en-us" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
  <meta charset="utf-8" />
  <title><?php echo $title; ?></title>
  <meta name="theme-color" content="#303030" />
  <meta name="msapplication-TileColor" content="#303030" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="google-site-verification" content="P_N2at6rFn26yfrkkp-ZZ7rSJs5uGiNU7zEX55hey5w" />
  <meta name="msvalidate.01" content="44880B2B9AD5C678175CB6D72A8070E2" />
  <meta name="yandex-verification" content="afa134893bec2635" />
  <meta name="robots" content="index, follow" />
  <link rel="preconnect" href="https://fonts.gstatic.com" />
  <link rel="preconnect" href="https://cdnjs.cloudflare.com" />
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&family=Material+Icons&display=swap" rel="stylesheet" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/2.0.46/css/materialdesignicons.min.css" rel="stylesheet" />
  <link rel="canonical" href="<?php echo $mainUrl . $_SERVER['REQUEST_URI']; ?>" />
  <meta property="og:type" content="website" />
  <meta name="twitter:title" content="<?php echo $title; ?>" />
  <meta property="og:title" content="<?php echo $title; ?>" />
  <meta property="og:url" content="<?php echo $mainUrl . $_SERVER['REQUEST_URI']; ?>" />
  <meta property="og:site_name" content="<?php echo $mainTitle; ?>" />
  <meta property="og:image" content="<?php echo $ogImage; ?>" />
  <meta name="twitter:image" content="<?php echo $mainUrl; ?>/static/images/twitter-image.jpg" />
  <?php if ($keywords):?>
    <meta name="keywords" content="<?php echo $keywords; ?>" />
  <?php endif;?>
  <?php if ($description):?>
    <meta name="description" content="<?php echo $description; ?>" />
    <meta property="og:description" content="<?php echo $description; ?>" />
  <?php endif;?>
  <link rel="manifest" href="/static/data/manifest.json" />
  <link rel="shortcut icon" href="/static/icons/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon" href="/static/icons/safari-pinned-tab.svg" type="image/svg+xml" />
  <!-- compiled css -->
  <style>.seo-block{position:fixed;clip:rect(0 0 0 0)}</style>
  <script type="application/ld+json">
  {
    "@context": "https://schema.org",
     "@type": "Website",
     "headline": "<?php echo $title; ?>",
     "alternativeHeadline": "<?php echo $title; ?>",
     "image": "<?php echo $ogImage; ?>",
     "editor": "<?php echo $me; ?>",
     "genre": "<?php echo $mainTitle; ?>",
     <?php if ($keywords):?>
     "keywords": "<?php echo $keywords; ?>",
     <?php endif;?>
     "publisher": "<?php echo $me; ?>",
     "url": "<?php echo $mainUrl . $_SERVER['REQUEST_URI']; ?>",
     "datePublished": "<?php echo date("Y-m-d"); ?>",
     "dateCreated": "<?php echo date("Y-m-d"); ?>",
     "dateModified": "<?php echo date("Y-m-d"); ?>",
     <?php if ($description):?>
     "description": "<?php echo $description; ?>",
     <?php endif;?>
     "author": {
      "@type": "Person",
      "name": "<?php echo $me; ?>"
    }
  }
  </script>
</head>
<body>
<div id="app"></div>
<div class="seo-block">
  <header>
    <h1><?php echo $mainTitle; ?></h1>
    <?php
    $heading = 2;
    foreach ($urlParts as $part) {
      if ($part) {
        echo '<h' . $heading . '>' . ucwords(str_replace('-', ' ', str_replace('%20', ' ', $part))) . '</h' . $heading . '>';
        $heading ++;
      }
    }
    ?>
  </header>
  <nav>
    <ul>
      <li>
        <a href="/" title="Home">Home</a>
      </li>
      <?php foreach ($categories as $category): ?>
        <li>
          <a href="/<?php echo $category; ?>" title="<?php echo ucfirst($category); ?>"><?php echo ucfirst($category); ?></a>
        </li>
      <?php endforeach; ?>
    </ul>
  </nav>
  <main>
    <?php
    $htmlFile = './static/pages/' . end($urlParts) . '.html';
    if (end($urlParts) && file_exists($htmlFile)) {
      include($htmlFile);
    } else if ($urlParts[1] === '') {
      include('./static/pages/index.html');
    } ?>
    <?php
      include('./links.php');
    ?>
  </main>
  <footer>&copy;<?php echo Date('Y');?> <a href="/"><?php echo $mainTitle; ?></a></footer>
</div>
<noscript>This website makes heavy use of JavaScript, which seems to be disabled in your browser.
  To use it, please, enable JavaScript and reload the page.</noscript>
<!-- compiled js -->
</body>
</html>
