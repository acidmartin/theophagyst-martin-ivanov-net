const fs = require('fs')

class Sitemap {
  constructor () {
    this.package = require('../package.json')
    this.routes = require('./routes.json').routes
    this.changefreq = 'weekly'
    this.priority = '1.0'
    this.domain = this.package.domain
    this.appName = this.package.appName
    this.htmlFilename = 'links.php'
    this.xmlFilename = 'sitemap.xml'
    this.parentDir = 'site-meta-files'
    this.html = []
    this.xml = []
  }
  generate () {
    this.routes.forEach(slug => {
      let label = slug.split('/').pop().replace(/-/g, ' ')
      const url = `${this.domain}/${slug}`
      if (!label) {
        label = this.appName
      }
      this.html.push(`<li><a href="/${slug}" title="${label}">${label}</a></li>`)
      this.xml.push(`\t<url>
    <loc>${url}</loc>
    <changefreq>${this.changefreq}</changefreq>
    <priority>${this.priority}</priority>
\t</url>\n`)
    })
    this.html.unshift('<ul>')
    this.html.push('</ul>')
    this.saveHTML()

    this.xml.unshift(`<?xml version="1.0" encoding="utf-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\n`)
    this.xml.push('</urlset>')
    this.saveXML()
  }
  saveHTML () {
    fs.writeFile(`${this.parentDir}/${this.htmlFilename}`, this.html.join(''), (err) => {
      if (err) {
        return console.log('Error', err)
      }
    })
  }
  saveXML () {
    fs.writeFile(`${this.parentDir}/${this.xmlFilename}`, this.xml.join(''), (err) => {
      if (err) {
        return console.log('Error', err)
      }
    })
  }
}

const sitemap = new Sitemap()
sitemap.generate()
